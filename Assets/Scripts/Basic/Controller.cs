﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    public float RumbleLength = 1;

    private LineRenderer lineRend;
    [SerializeField]private GameObject currentMovementPoint;
    [SerializeField]private GameObject Center;
    [SerializeField]private SteamVR_TrackedController leftController;
    [SerializeField]private SteamVR_TrackedController rightController;

    private Outline highlightedMovPoint = null;

	// Use this for initialization
	void Start ()
    {
        rightController.TriggerClicked += OnClick;
        lineRend = rightController.gameObject.GetComponent<LineRenderer>();
        lineRend.positionCount = 2;

        // Start at movement point
        TeleportPlayer(currentMovementPoint);
    }

    private void Update()
    {
        lineRend.SetPosition(0, rightController.transform.position);
        lineRend.SetPosition(1, rightController.transform.position + rightController.transform.forward * 10);

        RaycastHit hit;
        if (Physics.Raycast(rightController.transform.position, rightController.transform.forward, out hit, 100))
        {

            if (hit.transform.tag == "MovementPoint")
            {
                highlightedMovPoint = hit.transform.GetComponent<Outline>();
                highlightedMovPoint.enabled = true;
            }
            else
                highlightedMovPoint.enabled = false;
        }
        else
            highlightedMovPoint.enabled = false;


    }

    private void OnClick(object sender, ClickedEventArgs e)
    {
        RaycastHit hit;
        if (Physics.Raycast(rightController.transform.position, rightController.transform.forward, out hit, 100))
        {
            // Interact with UI
            if (hit.transform.GetComponent<Button>())
            {
                VibrateController();
                hit.transform.GetComponent<Button>().onClick.Invoke();
            }

            // Teleport to movement point
            if (hit.transform.tag == "MovementPoint")
            {
                VibrateController();
                currentMovementPoint.SetActive(true);
                TeleportPlayer(hit.collider.gameObject);
            }
        }
    }

    private void TeleportPlayer(GameObject newPosition)
    {
        //Calculates the player's offset in the VR playarea
        Vector3 playerOffset = new Vector3(
        Center.transform.localPosition.x - Camera.main.transform.localPosition.x,
        0,
        Center.transform.localPosition.z - Camera.main.transform.localPosition.z);

        Vector3 convertedTeleportPoint = Center.transform.InverseTransformPoint(newPosition.transform.position);
        convertedTeleportPoint = convertedTeleportPoint + playerOffset;

        transform.position = transform.TransformPoint(convertedTeleportPoint);
        currentMovementPoint = newPosition;
        currentMovementPoint.SetActive(false);
    }

    private void VibrateController()
    {
        StartCoroutine(VibrationLoop());
    }

    private IEnumerator VibrationLoop()
    {
        for (float i = 0; i < RumbleLength; i += Time.deltaTime)
        {
            SteamVR_Controller.Input((int)rightController.controllerIndex).TriggerHapticPulse(3999);
            yield return null;
        }

    }
}
