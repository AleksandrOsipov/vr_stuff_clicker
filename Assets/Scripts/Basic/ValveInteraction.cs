﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ValveInteraction : MonoBehaviour
{
    private GameObject _controller;
    private Vector3 _initialVectorToController;
    private float _initialRot = 0;

    public float _currentAngle;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            _controller = other.gameObject;
            _initialVectorToController = _controller.transform.position - transform.position;
            _initialVectorToController.Normalize();
            _initialRot = transform.eulerAngles.z;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _controller = null;
    }

    private void LateUpdate()
    {
        if(_controller != null)
        {
            Vector3 currentVectorToController = _controller.transform.position - transform.position;
            currentVectorToController.Normalize();

            _currentAngle = Vector3.SignedAngle(_initialVectorToController, currentVectorToController, transform.forward);
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, _initialRot + _currentAngle);
        }
    }
}
