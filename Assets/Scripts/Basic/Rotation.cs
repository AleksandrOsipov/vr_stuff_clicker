﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotation : MonoBehaviour {

    public float X, Y, Z;

    private void Update()
    {
        transform.Rotate(X, Y, Z); 
    }
}
