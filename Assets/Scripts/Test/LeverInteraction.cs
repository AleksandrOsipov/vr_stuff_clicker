﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverInteraction : MonoBehaviour {


    public float min = 0, max = 0;

    private float _leverOffset = 0, _contrOffset = 0;
    public SteamVR_TrackedController _controller = null;
    public bool moving = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            _controller = other.GetComponent<SteamVR_TrackedController>();
            _controller.TriggerClicked += Move;
            _controller.TriggerUnclicked += StopMove;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            _controller.TriggerClicked -= Move;
            _controller.TriggerUnclicked -= StopMove;
            _controller = null;
            moving = false;
        }
    }

    private void Move(object sender, ClickedEventArgs e)
    {
        moving = true;
        _contrOffset = _controller.transform.position.z;
    }

    private void StopMove(object sender, ClickedEventArgs e)
    {
        moving = false;

    }

    private void Update()
    {
        if (moving)
        {
            //transform.eulerAngles = new Vector3(Vector3.SignedAngle(transform.position, _controller.transform.position, Vector3.up), transform.rotation.y, transform.rotation.z);
            min = Vector3.SignedAngle(transform.position, _controller.transform.position, Vector3.zero);
        }
    }
}
