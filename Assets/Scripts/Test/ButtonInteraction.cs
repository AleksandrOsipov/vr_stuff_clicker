﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonInteraction : MonoBehaviour {

    public float min = 0, max = 0;

    private float _buttonOffset = 0, _contrOffset = 0;
    private SteamVR_TrackedController _controller = null;
    private bool moving = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            _controller = other.GetComponent<SteamVR_TrackedController>();
            _controller.TriggerClicked += MoveButton;
            _controller.TriggerUnclicked += StopMoveButton;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            _controller.TriggerClicked -= MoveButton;
            _controller.TriggerUnclicked -= StopMoveButton;
            _controller = null;
            moving = false;
        }
    }

    private void MoveButton(object sender, ClickedEventArgs e)
    {
        moving = true;
        _contrOffset = _controller.transform.position.z;
    }

    private void StopMoveButton(object sender, ClickedEventArgs e)
    {
        moving = false;

    }

    private void Update()
    {
        if (!moving)
            _buttonOffset = transform.position.z;
        else
        {
            Vector3 newPos = new Vector3(transform.position.x, 
                transform.position.y, 
                Mathf.Clamp(_buttonOffset + (_controller.transform.position.z - _contrOffset), min, max)); // to keep button position, not to snap to controllers position
            transform.position = newPos;
        }
    }
}
