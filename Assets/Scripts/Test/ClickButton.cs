﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickButton : MonoBehaviour {

    private bool _isRunning = false;
    private SteamVR_TrackedController _controller = null; 

    public Vector3 Up, Down;
    public float step = 0.003f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            _controller = other.GetComponent<SteamVR_TrackedController>();
            _controller.TriggerClicked += StartClick;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            _controller.TriggerClicked -= StartClick;
            _controller = null;
        }
    }

    private void StartClick(object sender, ClickedEventArgs e)
    {
        if (!_isRunning)
            StartCoroutine(Click());
    }

    IEnumerator Click()
    {
        _isRunning = true;
        StartCoroutine(HapticPulse());
        while (transform.position.y != Down.y)
        {
            transform.position = Vector3.MoveTowards(transform.position, Down, step);
            yield return new WaitForSeconds(0.01f);
        }

        while (transform.position.y != Up.y)
        {
            transform.position = Vector3.MoveTowards(transform.position, Up, step);
            yield return new WaitForSeconds(0.01f);
        }
        _isRunning = false;
    }

    IEnumerator HapticPulse()
    {
        for (int i = 0; i < 5; i++)
        {
            SteamVR_Controller.Input((int)_controller.controllerIndex).TriggerHapticPulse((ushort)3999);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
