﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlPanelUI : MonoBehaviour {

    const int MINPRESSURE = 0;
    const int MAXPRESSURE = 250;

    public Transform Arrow1, Arrow2;
    public GameObject BulbOn1, BulbOff1, BulbOn2, BulbOff2;

    #region BUTTONS
    public void Start1() { AirSystemManager.instance.Start1Compressor(); }
    public void Start2() { AirSystemManager.instance.Start2Compressor(); }
    public void Start3() { AirSystemManager.instance.Start3Compressor(); }
    public void Start4() { AirSystemManager.instance.Start4Compressor(); }

    public void Stop1() { AirSystemManager.instance.Stop1Compressor(); }
    public void Stop2() { AirSystemManager.instance.Stop2Compressor(); }
    public void Stop3() { AirSystemManager.instance.Stop3Compressor(); }
    public void Stop4() { AirSystemManager.instance.Stop4Compressor(); }
    
    public void Release1() { AirSystemManager.instance.Release1(); }
    public void Release2() { AirSystemManager.instance.Release2(); }
    #endregion

    public void Update()
    {
        float pres1 = AirSystemManager.instance.Pressure1;
        float pres2 = AirSystemManager.instance.Pressure2;
        UpdatePressure(pres1, pres2);
    }

    // Pass percentage to update pressure gauge
    private void UpdatePressure(float pressure1, float pressure2)
    {
        // On and Off bulbs if pressure is high
        if (pressure1 >= 80)
        {
            BulbOff1.SetActive(false);
            BulbOn1.SetActive(true);
        }
        else
        {
            BulbOff1.SetActive(true);
            BulbOn1.SetActive(false);
        }

        if (pressure2 >= 80)
        {
            BulbOff2.SetActive(false);
            BulbOn2.SetActive(true);
        }
        else
        {
            BulbOff2.SetActive(true);
            BulbOn2.SetActive(false);
        }

        // 1% of pressure is 1.4 value for gauge
        // -70-70=140 difference between arrow positions, 140/100% = 1.4
        float rot1 = pressure1 * MAXPRESSURE / 100;
        float rot2 = pressure2 * MAXPRESSURE / 100;

        Arrow1.localEulerAngles = new Vector3(0, rot1, 0);
        Arrow2.localEulerAngles = new Vector3(0, rot2, 0);
    }
}
