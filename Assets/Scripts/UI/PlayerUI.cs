﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour {

    public GameObject helpButton, helpPanel;

    public void ShowHelp()
    {
        helpButton.SetActive(false);
        helpPanel.SetActive(true);
    }

    public void HideHelp()
    {
        helpButton.SetActive(true);
        helpPanel.SetActive(false);
    }
}
