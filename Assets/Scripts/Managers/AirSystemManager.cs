﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirSystemManager : MonoBehaviour
{
    float COMPRESSOR_OUTPUT = 1;
    float RELEASE_OUTPUT = 5;

    public static AirSystemManager instance = null;

    // pressure is from 0 to 100, can be used as percentage
    [SerializeField]
    private float 
        _pressure1 = 0, 
        _pressure2 = 0;
    private bool
        _comp1 = false,
        _comp2 = false,
        _comp3 = false,
        _comp4 = false;
    private Coroutine
        _fill1 = null,
        _fill2 = null,
        _release1 = null,
        _release2 = null;

    #region UNITY FUNCTIONS
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }
    #endregion
    #region START COMPRESSORS
    public void Start1Compressor()
    {
        _comp1 = true;
        if (_fill1 == null)
            _fill1 = StartCoroutine(fill1());
    }

    public void Start2Compressor()
    {
        _comp2 = true;
        if (_fill1 == null)
            _fill1 = StartCoroutine(fill1());
    }

    public void Start3Compressor()
    {
        _comp3 = true;
        if (_fill2 == null)
            _fill2 = StartCoroutine(fill2());
    }

    public void Start4Compressor()
    {
        _comp4 = true;
        if (_fill2 == null)
            _fill2 = StartCoroutine(fill2());
    }
    #endregion
    #region STOP COMPRESSORS
    public void Stop1Compressor() { _comp1 = false; }
    public void Stop2Compressor() { _comp2 = false; }
    public void Stop3Compressor() { _comp3 = false; }
    public void Stop4Compressor() { _comp4 = false; }
    #endregion
    #region RELEASE PRESSURE
    public void Release1() { StartCoroutine(release1()); _comp1 = false; _comp2 = false; }
    public void Release2() { StartCoroutine(release2()); _comp3 = false; _comp4 = false; }
    #endregion

    public float Pressure1 { get { return _pressure1; } }
    public float Pressure2 { get { return _pressure2; } }

    #region COROUTINES
    // Gradually adds pressure depending on number of running compressors
    IEnumerator fill1()
    {
        // If no compressors are running stop coroutine
        if (!_comp1 && !_comp2)
        {
            _fill1 = null;
            yield break;
        }

        yield return new WaitForSeconds(0.1f);
        _pressure1 += (_comp1 ? COMPRESSOR_OUTPUT : 0) + (_comp2 ? COMPRESSOR_OUTPUT : 0);
        _pressure1 = (int)Mathf.Clamp(_pressure1, 0, 100);
        StartCoroutine(fill1());
    }

    // Gradually adds pressure depending on number of running compressors
    IEnumerator fill2()
    {
        // If no compressors are running stop coroutine
        if (!_comp3 && !_comp4)
        {
            _fill2 = null;
            yield break;
        }

        yield return new WaitForSeconds(0.1f);
        _pressure2 += (_comp3 ? COMPRESSOR_OUTPUT : 0) + (_comp4 ? COMPRESSOR_OUTPUT : 0);
        _pressure2 = (int)Mathf.Clamp(_pressure2, 0, 100);
        StartCoroutine(fill2());
    }

    IEnumerator release1()
    {
        yield return new WaitForSeconds(0.1f);
        _pressure1 -= RELEASE_OUTPUT;
        _pressure1 = (int)Mathf.Clamp(_pressure1, 0, 100);
        if (_pressure1 > 0)
            _release1 = StartCoroutine(release1());
        else
            _release1 = null;
    }

    IEnumerator release2()
    {
        yield return new WaitForSeconds(0.1f);
        _pressure2 -= RELEASE_OUTPUT;
        _pressure2 = (int)Mathf.Clamp(_pressure2, 0, 100);
        if (_pressure2 > 0)
            _release2 = StartCoroutine(release2());
        else
            _release2 = null;
    }
    #endregion
}
